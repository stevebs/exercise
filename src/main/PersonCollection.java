package main;

import java.util.LinkedList;
import java.util.Observable;

public class PersonCollection extends Observable{
	private PersonMatcher pm;
	private LinkedList<Person> persons;
	
	//demanding person matcher alg for starting
	public PersonCollection(PersonMatcher pm){
		this.pm=pm;
		this.persons=new LinkedList<Person>();
	}

	/*
	 * locks the data structure for other threads (thread-safe)
	 * send the data to the algorithm
	 * print data structure
	 * publish it to observers
	 * Complexity : o(n)
	 */
	public void add(Person arg) {
		synchronized (persons){
			if(pm.add(persons, arg)){
				System.out.println(arg.getName()+" has been added.");
				printAllP();
				this.publish(arg.getName()+" has been added.");
			}
		}

	}

	/*
	 * locks the data structure for other threads (thread-safe)
	 * remove the person with the maximum value
	 * Complexity: O(1)
	 */
	public Person remove() {
		//Complex : O(1)
		synchronized (persons){
			if(persons.isEmpty())
				return null;
			Person tmp = persons.getFirst();
			persons.removeFirst();
			System.out.println(tmp.getName()+" has been deleted.");
			printAllP();
			this.publish(tmp.getName()+" has been added.");
			return tmp;
		}
	}
	//notify observers for any change and send a msg
	private void publish(String msg){
		this.setChanged();
		this.notifyObservers(msg);
	}
	//descending
	
	//helper function
	private void printAllP() {
		System.out.println("Persons in Collection (descending):");
		for(int i=0; i<persons.size();i++)
		{
			System.out.print(persons.get(i).getName()+"-->");
		}
		System.out.println("");
	}

}
