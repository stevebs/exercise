package main;
import java.util.concurrent.TimeUnit;
public class Main {

	public static void main(String[] args) {
		PersonCollection pc = new PersonCollection(new SimpleMatchAlg(new PersonSalaryComperator()));
		/*
		 * test cases
		 * the first 2 insertion is for the data structure to not be empty
		 */
		Test t = new Test();
		t.addTestCase(new Thread(){
			public void run(){
				pc.add(new NicePerson("john",150));
			}
		});
		t.addTestCase(new Thread(){
			public void run(){
				pc.add(new NicePerson("Kelly",200));
			}
		});
		t.runAll();
		
		
		//sleep for 1 sec
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		/*
		 * Concurrency check 
		 * add and remove threads
		 */
		t.addTestCase(new Thread(){
			public void run(){
				pc.remove();
			}
		});
		t.addTestCase(new Thread(){
			public void run(){
				pc.add(new NicePerson("Mike",160));
			}
		});
		t.addTestCase(new Thread(){
			public void run(){
				pc.add(new NicePerson("Rachel",155));
			}
		});
		t.addTestCase(new Thread(){
			public void run(){
				pc.remove();
			}
		});
		t.addTestCase(new Thread(){
			public void run(){
				pc.remove();
			}
		});
		t.addTestCase(new Thread(){
			public void run(){
				pc.remove();
			}
		});
		t.runAll();
	}

}
