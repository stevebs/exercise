package main;

import java.util.LinkedList;
//Concrete matching algorithm
public class SimpleMatchAlg extends GeneralMatcher {

	public SimpleMatchAlg(PersonComperator pc) {
		super(pc);
	}
	@Override
	public boolean add(LinkedList<Person> persons, Person p) {
			if(persons.isEmpty()){
			persons.add(p);
			return true;
		}
		int x=this.pc.compare(p, persons.getFirst());
		if(x==-1){
			persons.addFirst(p);
			return true;
		}
		for(int i=1;i<persons.size();i++){
			if(this.pc.compare(p, persons.get(i-1))==1&&this.pc.compare(p, persons.get(i))<=0){
				persons.add(i, p);
				return true;
			}
		}
		persons.addLast(p);
		return true;
	}


}
