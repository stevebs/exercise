package main;

import java.util.Vector;
//a test class to run the threads for testing concurrency
public class Test {
	Vector<Thread> vt;
	public Test(){
		vt=new Vector<Thread>();
	}
	
	public void addTestCase(Thread t){
			vt.add(t);
	}
	public void runAll(){
		while(!vt.isEmpty()){
			Thread t=vt.get(0);
			vt.remove(0);
			t.start();
		}
	}
}
