package main;
//a specific salary comparator
public class PersonSalaryComperator implements PersonComperator {

	@Override
	public int compare(Person a, Person b) {
		if(a.getSalary()>b.getSalary())
			return -1;
		else if(a.getSalary()==b.getSalary())
			return 0;
		else
			return 1;
	}

}
