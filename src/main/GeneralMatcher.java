package main;
//a common class among different matcher;
public abstract class GeneralMatcher implements PersonMatcher {
	protected PersonComperator pc;
	//for each instance of matcher a comparator is required 
	public GeneralMatcher(PersonComperator pc){
		this.pc=pc;
	}
}
