package main;

public interface Person {
	public int getSalary();
	public String getName();
}
