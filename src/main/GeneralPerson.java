package main;
//an example for a person 
public class GeneralPerson implements Person {
	protected int salary;
	protected String name;
	protected GeneralPerson(String name,int sal){
		this.name=name;
		this.salary=sal;
	}
	public int getSalary() {
		return this.salary;
	}
	public String getName() {
		return this.name;
	}
}
