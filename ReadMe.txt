check out branch release from the rep.

Interfaces:
Person
PersonMatcher
PersonComparator

Logical Concept:
PersonCollection is a managing class, with the use of the List data structure and the accepted matching algorithm.
the matching algorithm initialization require a comparator or it uses a default one.
thus not only the PersonCollection class doesn't know who the algorithm is nor how he compare different objects.
supports:
-add
-remove
-publish

add:
this function use the initialize matching algorithm adding function.
**the adding algorithm function save the object by descending order with the use of the comparator.
thus yielding a sorted data structure without knowing how or by what.
insertion time: Worst :O(n)
remove:
remove the head of the structure.
remove time : O(1)
publish:
extend the observable class, activing the set change and notify functions when adding or removing (with a string msg for a specific group).
if an observer such as publisher gets notify and he will send the msg.

thread-safe:
the data structure requires a lock because he is the concreate data for us.
when adding or removing, the first thread locks the data structure thus no other threads are able to change it.
